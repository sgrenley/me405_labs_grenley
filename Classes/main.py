'''@file main.py
@author Spencer Grenley
@date May 5 2020
'''

from Driver import MotorDriver
from Encoder import MotorEncoder
import pyb

class main:
    '''This class performs closed-loop proportional control on a sytem.'''

    def __init__(self, setpoint, kp):
        '''This method initiates the given parameters to create an instance of main (a controller). 
        @param self.sp The desired point for the controller to reach (i.e. final position).
        @param self.kp The desired proportional gain constant that will be multiplied against the error signal to
        create the closed loop gain.'''

        self.sp = setpoint 
        self.kp = kp
        
        '''These parameters will be used in the update and gain methods'''
        self.es = self.sp #Error Signal, set to setpoint originally (assumes current position is 0)
        self.gain = 0     #Creates gain var, set initially to zero
    
        
    def update(self,cp):
        '''When called, this method takes the current position and updates/returns the error signal
        @param cp The user's input for whatever they would like to refer to as 'the current position' (i.e. encoder position)
        @param self.es The calculated error signal'''
        
        encoder1.update()
        self.es = self.sp - cp
        return self.es
        
    def find_gain(self):
        '''This method calulates/returns the gain from the current error signal value
        This method also prevents that value from exceeding abs(100) for use with duty cycles
        @param self.gain The calculated gain for the system, which is then returned (i.e. duty cycle)'''
        
        self.gain = self.es*self.kp
        if self.gain > 100:
            self.gain = 100
        elif self.gain < -100:
            self.gain = -100
        return self.gain
        



if __name__ == '__main__':
    
    '''Create Encoder Instance'''
    ENCA_pin = pyb.Pin(pyb.Pin.cpu.B6)
    ENCB_pin = pyb.Pin(pyb.Pin.cpu.B7)
    tim_period = 65535 #Period length for 16 bit counter
    tim_delay_ms = 10 #Delay in miliseconds
    
    #Encoder pins PB6 & PB7 use tim 4 for channels 1 & 2
    #Encoder pins PC6 & PC7 use tim 8 for channels 1 & 2
    tim_enc = pyb.Timer(4, prescaler=0, period= tim_period) #Using PB6 and PB7
    
    #Initialize encoder instance
    encoder1 = MotorEncoder(ENCA_pin, ENCB_pin, tim_period, tim_delay_ms, tim_enc)
    
    '''Create Driver Instance'''
    pin_EN = pyb.Pin (pyb.Pin.board.PA10,pyb.Pin.OUT_PP)
    # IN pins don't need to be set as an high/low pin - they will be used for pwm
    pin_IN1 = pyb.Pin (pyb.Pin.board.PB5)
    pin_IN2 = pyb.Pin (pyb.Pin.board.PB4)
    # Create the timer object used for PWM generation
    tim_driv = pyb.Timer(3, freq = 20000)
    # Timer (timer channel, freq)
    # Create a motor object passing in the pins and timer
    moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim_driv)

    # Enable the motor driver
    moe.enable()
    
    '''Create control loop paramters'''   
    kp = .05 #Desired Kp value
    setpoint = 10000 #Desired location of the motor
    controller1 = main(setpoint,kp) #Create controller instance
    
    #Initiate paramters to be later used in printing data
    t=0
    i=1
    duty = 10 #sets initial duty cycle to get motor spinning
    
    while True:
        pyb.delay(tim_delay_ms) #Creates the time delay (set above)
        
        encoder1.update()
        moe.set_duty(duty)
        controller1.update(encoder1.getposition())
        duty = controller1.find_gain()
    
        '''Print out data to be copied into excel'''
        t = t+tim_delay_ms/1000 #Seconds
        #Printed data = (time, encoder position, duty)
        if i < 150: #only prints data for 150 cycles
            print('{:},{:},{:}'.format(t,encoder1.getposition(),duty))
        i=i+1
        
            
        
        
        
        


            
        
