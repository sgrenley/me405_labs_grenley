var classEncoder_1_1MotorEncoder =
[
    [ "__init__", "classEncoder_1_1MotorEncoder.html#a152a8b7ded7e520323025c114a758648", null ],
    [ "getdelta", "classEncoder_1_1MotorEncoder.html#a0aa5d0457a4329a9b3e35b21e0cb4207", null ],
    [ "getposition", "classEncoder_1_1MotorEncoder.html#a683c7a3892628cd91ea44236ebd0f794", null ],
    [ "setposition", "classEncoder_1_1MotorEncoder.html#a6f70e6347fb64cda5567a4effb22878c", null ],
    [ "update", "classEncoder_1_1MotorEncoder.html#aa38e7740ee60649c294588f222ac8210", null ],
    [ "delay_ms", "classEncoder_1_1MotorEncoder.html#a2fee79110ea63739a8cce282ff506a20", null ],
    [ "ENCA_pin", "classEncoder_1_1MotorEncoder.html#a0a7461bccc02491c22d6d55508448522", null ],
    [ "ENCB_pin", "classEncoder_1_1MotorEncoder.html#aa65935c88d688439408614f1bdc69534", null ],
    [ "period", "classEncoder_1_1MotorEncoder.html#a3658dfe43e6bab89a286e7333d20da9b", null ],
    [ "pos_corrected", "classEncoder_1_1MotorEncoder.html#a906cb6bf8b8aac40439dfcc98c9bf035", null ],
    [ "pos_raw", "classEncoder_1_1MotorEncoder.html#a51f450e96ef0ed70998df1d3d8f74f13", null ],
    [ "pos_raw_previous", "classEncoder_1_1MotorEncoder.html#ab74800cfb66aa6a0fb34c62bdef6ccdf", null ],
    [ "position_new", "classEncoder_1_1MotorEncoder.html#a6552a8bc2a1b9de62ca016ba82d87fd1", null ],
    [ "timch1", "classEncoder_1_1MotorEncoder.html#af736a2d562d8dddcf59874bc0926e41d", null ],
    [ "timch2", "classEncoder_1_1MotorEncoder.html#aa8fc16d47c4def519503d2612a4e240d", null ],
    [ "timer", "classEncoder_1_1MotorEncoder.html#ae717bc869e5b3f608c3c4d6fe4d673f6", null ]
];