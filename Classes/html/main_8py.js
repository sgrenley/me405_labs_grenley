var main_8py =
[
    [ "main", "classmain_1_1main.html", "classmain_1_1main" ],
    [ "controller1", "main_8py.html#a804494a357139d78e7983b154b86e37b", null ],
    [ "duty", "main_8py.html#a67514afbf4ef7ef133a8277edc2c29e3", null ],
    [ "ENCA_pin", "main_8py.html#aac5d2884728c6ddee2ba0a4b8c7e6e75", null ],
    [ "ENCB_pin", "main_8py.html#ab964c1984c3140547dfb64f7e61a3ec4", null ],
    [ "encoder1", "main_8py.html#aba9d9b2352ce59c6a1f84ee47dc46dd6", null ],
    [ "i", "main_8py.html#ade6e2febf33b88a767c2e85902add210", null ],
    [ "kp", "main_8py.html#aeb23622e6e94b907d17455a2e972765f", null ],
    [ "moe", "main_8py.html#a8f2f54089b569c0054ca1f819f855a5d", null ],
    [ "pin_EN", "main_8py.html#a285cbf8b3524ac6ad9e6854993d37516", null ],
    [ "pin_IN1", "main_8py.html#a537d3d4da75a61c087ac80386962f99e", null ],
    [ "pin_IN2", "main_8py.html#afb0a14e68f5dcbaa02d2d01bcd5bc447", null ],
    [ "setpoint", "main_8py.html#a424f2bc8c95a3a4452e0468c1f252aed", null ],
    [ "t", "main_8py.html#a1ad9787d62d88c0733d6445ac104a8ca", null ],
    [ "tim_delay_ms", "main_8py.html#a5adff7ea4a7d03f9a2af619131e80461", null ],
    [ "tim_driv", "main_8py.html#aae36887547748b5941d915a02782b658", null ],
    [ "tim_enc", "main_8py.html#a7aa3562e2c1762a9cf8120f7c6f7da26", null ],
    [ "tim_period", "main_8py.html#a69affdbcad0c694ada897fe07c0a4211", null ]
];