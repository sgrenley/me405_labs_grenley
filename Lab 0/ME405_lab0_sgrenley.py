"""
@file ME405_lab0_sgrenley.py
@date: Created on Wed Apr 17 21:44:55 2020
@author: spenc
"""
import sys #Gives access to system kill switch that can be used from within a function

def get_index():
# @author Spencer Grenley
# @param nth_idx : The user's desired index value
    
# This function:
   #Prompts the user to input their desired index
   #Checks to ensure input is a valid integer (redirect to prompt if not)

    while True:     #Allows 'continue' to be used
# INPUT: Determine what index value user wants
        print('WARNING: Inputs over 10^7 may crash your computer')
        nth_idx = input('What is the index for your desired Fibonacci number? -->  ')
 
# Determine if first input is a valid positive integer 
        try:
            int(nth_idx)
        except ValueError:
            print('ERROR: Invalid input! Please input a positive integer value')
            continue
        else:
            return (nth_idx)
        
def get_ts():
# @author Spencer Grenley
# @param ts : The user input that will either be a "t"  or "s" (term or sequence)

# This function:
    #Prompts user to input if they want the full sequence (s), or just the term (t)
    #Checks to ensure input is valid (redirect to prompt if not)

    while True:
 # INPUT: Determine if user wants the full sequence or just the nth term
        print('Would you like the full sequence up to your term,' 
              'or just the nth term?')
        ts = input('(s)equence or (t)erm? -->  ')
       
      # Determine if input 2 is a valid input ('s' or 't')
        if ts == 's':
            print('Corresponding Fibonacci Sequence:')
            return (ts)
        elif ts == 't':
            print('Corresponding Fibonacci Term:')
            return (ts)
        else:
            print('ERROR: Invalid response. Please input "s" or "t" ')
            continue
    
def fib(nth_idx,ts):
# @author Spencer Grenley


# NOTE: I used a sample code by Programiz as reference for this function (link below)
# @reference https://www.programiz.com/python-programming/examples/fibonacci-sequence
    
#This function:
    #Takes user index, calculates the Fibonacci sequence up to that term
    #Outputs the sequence or term (depending on user's input)


# @param n2 : represents the "n-2" term in the sequence
# @param n1 : represents the "n-1" term in the sequence
# @param count: keeps the while loop moving forward
# @param seq : array that stores the Fibonacci values from the while loop


    
    n2,n1 = 0,1     #Fist 2 terms of the sequence
    
    count = 0       #Sets counter to zero
    seq=[]          #Creates blank array to capture loop data
    
    if nth_idx == 1:
        print(n2)
    elif nth_idx == 2:
        if ts == 't':  # user just wants the nth term
            print(n1)
        else:                    # user wants entire sequence   
            print(n2,n1)
    else:   # For n >= 2
        while count < nth_idx :
        # Calculate the 3rd term (first summation using n-2 and n-1)
            seq.append(n2)
            nth = n1 + n2 
        # Calculate all terms for n > 3
        # Shift values over to start looping (until count reaches nth_idx)
            n2= n1
            n1= nth
            nth = n1 + n2
            count +=  1
        if ts == 't':  # user just wants the nth term
            print(seq[nth_idx-1]) #must -1, seq ends 1 less than idx
        else :         # user wants entire sequence- ts must be "t" or "s"
            print(seq) 
            
def user_cont():
#This function:
    #Asks user if they wish to input another index ('go again'), or quit
    #If they wish to go again, lets infinite main loop run another cycle
    #If they wish to quit, initiate kill switch from within function (sys.exit)
    
#INPUT: Determine if user wants to go again, or quit
    while True:
        user_cont= input('Do you want to try another value? (y)es or (n)o? --> ')
        if user_cont == 'y':
            print ('Uno Mas!')
            break           #Exits back into main infinite loop
        elif user_cont == 'n':
            print('Alrighty then, see ya later!')
            sys.exit(0)     #System kill switch (from "sys" library)
        else:
            print('ERROR: Invalid response! Please input a "y" or "n" --> ')
            continue        #Jumps back to the start of this loop to prompt user again
        
        
#-----------------------------MAIN LOOP---------------------------------------

if __name__ == '__main__':

    while True:
#Prompt the user to input their desired index
   #Check to ensure it is a valid integer value (redirect to prompt if not)
        user_index = get_index()
        
#Prompt user to input if they want the full sequence (s), or just the term (t)
    #Check to ensure input is valid (redirect to prompt if not)
        user_ts = get_ts()
        
#Take user index, calculate the Fibonacci sequence up to that term
    #Output the sequence or term (depending on user's input)
        fib(int(user_index),user_ts)

#Ask user if they wish to input another index ('go again'), or quit
    #If they wish to go again, let endless loop keep running
    #If they wish to quit, initiate kill switch from within function (sys.exit)
        user_cont()
    

    

        
           

        
        
        