'''@file mainpage.py
@author Spencer Grenley
@date May 5 2020

@mainpage
@section intro Introduction
This site documents the MotorEncoder class created for the ME405 Spring 2020 Lab.
The intent of this page is to include details to allow another user to understand
the code written, especially for when creating instances of this class.

@section Purpose
The MotorEncoder class defines the attributes of and Encoder object that interfaces
with the physical quadrature encoder on the DC motor.
The end goal of having this encoder is to use the information it contains about the
motor position and speed to use in a feedback control loop of the motor.

@section Useage
This class creates an encoder object that recieves the two phase encoder information,
and transfers it to two ENC pins with the appropriate timer channels. These two pins
represent the state of the two phases on the physical quadrature encoder.

To set these pins, the user must choose between either the PC6&PC7 pairing, or the
PB6&PB7 pairing. The timers for either set to create a channel 1/ channel 2 input are
timer 8 and timer 4, respectively (according to STM3L2L476RG data sheet).
The timer serves as the counter for the encoder class.

ThisThis class possesses the code to correct for over flow or under flow, and ultimately
output a current position and corrected delta value. Lastly, the class possesses a
method that allows the user to reset the postion to a desired value.

@section Testing
As of 5/5/20, the MotorEncoder class has only been tested via hand turning a single
DC motor. More testing is to come.

@section Location
The link to the code's location in my repository is:
https://bitbucket.org/sgrenley/me405_labs_grenley/src/master/Lab%202/Encoder.py '''