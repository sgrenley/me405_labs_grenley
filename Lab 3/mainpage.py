'''@file mainpage.py
@author Spencer Grenley
@date May 13 2020

@mainpage
@section intro Introduction
This site documents the main.py class created for the ME405 Spring 2020 Lab.
The intent of this page is to include details to allow another user to understand
the code written, especially for when creating instances of this class.

@section Purpose
The main.py class defines the attributes of a controller object, that creates a closed loop
PID controller. The goal of this class is to be used with the previously created Encoder and Driver classes.
However, this class is designed to serve as a general controller for any system. 

@section Useage
For use as a motor controller, this class recieves the users desire setpoint and Kp value. It utilizes the encoder
position to calculate an error signal, and then sends back the calculated gain (which, in this case, is a duty % to
be sent to the motor driver).

To be able to import any classes you wish use in the controller (i.e. Driver and Encoder), those files must
be saved onto the microcontroller, in addition to the main.py file.
(i.e. saved files are main.py, Driver.py, Encoder.py)

To set up the Encoder object, using a Nucleo board: 
The user must choose between either the PC6&PC7 pairing, or the
PB6&PB7 pairing. The timers for either set to create a channel 1/ channel 2 input are
timer 8 and timer 4, respectively (according to STM3L2L476RG data sheet).
The timer serves as the counter for the encoder class.

To set up the Driver object, using a Nucleo board:
The user must set up an EN and 2 IN pins. There are two available sets with the Nucleo board and motor driver:
[PA10 (EN), PB4, PB5] & [PC1 (EN), PA0, PA1]

@section Testing
As of 5/13/20, this class has been fully tested with 1 motor/encoder. The desirable Kp value was found through plotting
several responses, each with a different Kp value. The criteria to select a Kp value was to find a response that was relatively 
quick with no overshoot. The different responses with 4 trial Kp values can be seen below.

@image html positionplot.JPG width=(20)%

The Kp value of 0.05 was chosen due to it best satisfying the aformentioned criteria (no overshoot, and relatively quickly reaches
steady state). The duty and position were then plotted together over time, and can be seen in the figure below.

@image html chosenkp.JPG width=(20)%

A short video was taken of the motor at the selected Kp value of 0.05 - Use the link to view <BR>
https://cpslo-my.sharepoint.com/:v:/g/personal/sgrenley_calpoly_edu/EZ34gLJja3VKhfPhvUHTDKcBWD-FAMSXTasKHZes6-9jgQ?e=ZMuN4s

@section Location
The link to the code's location in my repository is:
https://bitbucket.org/sgrenley/me405_labs_grenley/src/master/Lab%203/main.py'''

