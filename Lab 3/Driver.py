"""@file Driver.py
@author Spencer Grenley
@date 4/24/20
"""
import pyb

class MotorDriver:
    ''' This class implements a motor driver for the ME405 board. '''
    
    def __init__ (self, EN_pin, IN1_pin, IN2_pin, timer):
        ''' Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety
        @param EN_pin A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2
        @param timer A pyb.Timer object to use for PWM on IN1_pin and IN2_pin
        '''
        
        print ('Creating a motor driver...') #Method initiated
        
    # self.blank created an "object variable" (can access in all methods)
            #self.EN_pin is available for all methods in class
            #EN_pin is just local to init method
            #However, both refer to the same thing
 
        # EN is a GPIO (Gen Purpose Input Output) - set to be PP output
        self.EN_pin = EN_pin 
        self.IN1_pin = IN1_pin
        self.IN2_pin = IN2_pin
    
        self.timer = timer
        
    # Create timer channels for IN1 and IN2 (for use in PWM)
        self.timch1 = self.timer.channel(1, pyb.Timer.PWM, pin=self.IN1_pin)
        self.timch2 = self.timer.channel(2, pyb.Timer.PWM, pin=self.IN2_pin)
        
        #Sets up all the data we need, then turns motor driver off for safety
        self.EN_pin.low()
        
        print ('Motor driver created!') #Method success!

    def enable (self):
        """This method enables the motor driver"""
        print ('Enabling Motor...')
        self.EN_pin.high()
        print ('Motor Enabled!')
        
    def disable (self):
        """ This method disables the motor driver"""
        print ('Disabling Motor...')
        self.EN_pin.low()
        print ('Motor disabled!')

    def set_duty (self, duty):
        
        """ This method sets the duty cycle to be sent
        to the motor to the given level. Positive values
        cause effort in one direction, negative values
        in the opposite direction.
        @param duty A signed integer holding the duty
        cycle of the PWM signal sent to the motor """
        
        if duty > 0 and duty <=100: #Duty is (+)
            self.timch1.pulse_width_percent(0)
            #Set it up as a pulse width modulation (timer channel)
            #Effectively sets IN1 to "low"
            self.timch2.pulse_width_percent(abs(duty))
            # PWM needs (+) value --> abs value of "duty"
            # switching channels accounts for (-) and reverses motor
            #print('Motor spinning counter-clockwise')
                
        elif duty <0 and duty >=-100: #Duty is (-)
            self.timch2.pulse_width_percent(0)
            #Set it up as a pulse width modulation (timer channel)
            #Effectively sets IN2 to "low"
            self.timch1.pulse_width_percent(abs(duty))
            #print('Motor spinning clockwise')
                
        elif duty >100:
            #print('Duty is too high! Motor needs duty < abs(100) to spin')
            duty = 100
        elif duty <-100:
            #print('Duty is too high! Motor needs duty < abs(100) to spin')
            duty = -100
            
    
                
        """if duty >= 6 and duty <=100: #Duty is (+)
                self.timch1.pulse_width_percent(0)
                #Set it up as a pulse width modulation (timer channel)
                #Effectively sets IN1 to "low"
                self.timch2.pulse_width_percent(abs(duty))
                # PWM needs (+) value --> abs value of "duty"
                # switching channels accounts for (-) and reverses motor
                print('Motor spinning clockwise')
                
        elif duty <=-6 and duty >=-100: #Duty is (-)
                self.timch2.pulse_width_percent(0)
                #Set it up as a pulse width modulation (timer channel)
                #Effectively sets IN2 to "low"
                self.timch1.pulse_width_percent(abs(duty))
                print('Motor spinning counter-clockwise')
                
        elif duty >-6 and duty <6:
                print('Duty is too low! Motor needs duty > abs(6) to spin')
        elif duty >100 or duty <-100:
                print('Duty is too high! Motor needs duty < abs(100) to spin')
        else:
                print('ERROR: Duty input is invalid')"""
   
if __name__ == '__main__':
# Adjust the following code to write a test program for your motor class. Any
# code within the if __name__ == '__main__' block will only run when the
# script is executed as a standalone program. If the script is imported as
# a module the code block will not run.

# Create the pin objects used for interfacing with the motor driver
    # EN is a GPIO (Gen Purpose Input Output) - set to be "Push Pull output"
    pin_EN = pyb.Pin (pyb.Pin.board.PA10,pyb.Pin.OUT_PP)
                           #(creates pin, output to motor driver)
    
    # IN pins don't need to be set as an high/low pin - they will be used for pwm
    pin_IN1 = pyb.Pin (pyb.Pin.board.PB5)
    pin_IN2 = pyb.Pin (pyb.Pin.board.PB4)

# Create the timer object used for PWM generation
    tim = pyb.Timer(3, freq = 20000)
    # Timer (timer channel, freq)

# Create a motor object passing in the pins and timer
    moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)

# Enable the motor driver
    moe.enable()

# Uses set_duty method to set the duty cycle of the PWM signal

    # INPUT A NUMERICAL VALUE!
    # MAX: abs(100)
    # MIN: abs(6)
    # (+): Counterclockwise (PEG)
    # (-): Clockwise (PEG)
    
  # moe.set_duty(duty)
    moe.set_duty(-40)
  