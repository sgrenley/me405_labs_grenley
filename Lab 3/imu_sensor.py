'''file imu_sensor.py

BNO055 IMU
Script includes a test program in the main loop, that only runs
if script is executed as a stand alone program.
@author Spencer Grenley
@date 5/20/20'''

import pyb

class IMU:
    
    def __init__(self,master_object,slave_address):
        
        '''This method initializes the parameters needed to create an instance of the IMU object.
        @param self.i2c The master device or object, created by the user in the main loop
        @param self.address The register address of the slave, in hexidecimal, that will be used for communication.'''
        self.i2c = master_object
        self.address = slave_address
        
        '''Registers for Bosch BNO055 IMU'''
        #Operating modes
        self.CONFIGMODE = 0x00
        self.IMU = 0x08
        self.NDOF = 0x0C
        
        #Addresses
        self.CALIB_STAT = 0x35 #1 byte, SYS<7:6>, GYR<5:4>, ACC<3:2>, MAG<1:0>
        self.OPR_MODE = 0x3D   #1 byte, only bits <3:0> are accessible
        
        #For all of the Euler data - 6 bytes
        self.EUL_DATA_HRP = 0x1A   #Heading: [0] LSB, [1] MSB
                                   #Roll:    [2] LSB, [3] MSB
                                   #Pitch:   [4] LSB, [5] MSB
        
        #For all of the Gyro data - 6 bytes
        self.GYR_DATA_XYZ = 0x14  #X: [0] LSB, [1] MSB --> d/dt Pitch
                                  #Y: [2] LSB, [3] MSB --> d/dt Roll
                                  #Z: [4] LSB, [5] MSB --> d/dt Heading
      
    def signed(n):
        '''This method accounts for python not accepting signed data, despite that 
        being what the IMU outputs. This method takes an unsigned 16 bit integer and
        converts it into a signed integer. NOTE: This method is for internal "behind the scenes" use only.
        It is for use in other methods. It cannot be called upon externally as a stand-alone method.  
        @param n The unsigned integer or list of integers the user wishes to sign.
        @param M The the outputted list of signed integer(s).'''
        
        try:
            len(n)  #Determines if "n" is a list or a single integer
        except TypeError: #"n" is a single integer value
            length=1 #the loop will now only go through one time
            n=(n,0)  #Turns the single integer value of "n" into a list that is subscriptable.
                     #0 is just a placeholder - the loop will only go through once, & only use "n"
        else:
            length=len(n)
            
        period = 255 #Max integer value stored within a byte
        i=0
        M=[] #output list of signed integer(s)
        
        while i<length:
            if n[i]>=128:
                #n should be a negative integer
                M.append(n[i]-period) #correct the value of n
            else:
                #n is a positive integer
                M.append(n[i])
            i+=1
        return M
    
    def enable(self):
        '''This method enables the sensor by setting the operating mode to "Nine Degrees of Freedom" (NDOF)'''
        self.i2c.mem_write(self.NDOF,self.address,self.OPR_MODE)
        print('IMU Enabled')
    
    def disable(self):
        '''This method disables the sensor by setting the operating mode to "configuration mode" (CONFIGMODE)'''
        self.i2c.mem_write(self.CONFIGMODE,self.address,self.OPR_MODE)
        print('IMU Disabled - set to configuration mode')
        
        
    def setmode(self,mode):
        '''This method allows the user to set the operating mode of the sensor. Note that the device must be
        disabled (in configuration mode) for the mode to be changed.
        @param mode The users desired operating mode for the sensor.'''
        self.i2c.mem_write(mode,self.address,self.OPR_MODE)
        print('IMU mode set')
        
                           
                           
    def calib_status(self):
        '''This method takes outputs the calibration status of the sensor in the following order:
        Magnetrometer, Accelerometer, Gyroscope, System.
        @param data The raw calibration status data from the sensor
        @mag_cal The calibration status of the magnetrometer.
        @acc_cal The calibration status of the accelerometer.
        @gyr_cal The calibration status of the gyroscope.
        @sys_cal The calibration status of the system.'''
        
        data = self.i2c.mem_read(1,self.address,self.CALIB_STAT)
        mag_cal = (data[0]) & 0b11
        acc_cal = (data[0] >> 2) & 0b11
        gyr_cal = (data[0] >> 4) & 0b11
        sys_cal = (data[0] >> 6) & 0b11
        
        return (mag_cal,acc_cal,gyr_cal,sys_cal)
        
    def euler_angle(self):
        '''This method outputs a tuple containing the current value of each Euler angle (in degrees). 
        The order is (Heading, Roll, Pitch).
        @D The raw data output of the sensor
        @euler_velocity A tuple containing the signed and converted angular velocities of the Heading, Roll, & Pitch (in degrees/sec)'''
        D = self.i2c.mem_read(6,self.address,self.EUL_DATA_HRP) #Raw Data
        #"D" is a 6 byte list of raw Euler data
        #Heading: [0] LSB, [1] MSB
        #Roll:    [2] LSB, [3] MSB
        #Pitch:   [4] LSB, [5] MSB
        
        #Run list, D, through the signed method to sign integers
        D=IMU.signed(D)
           
        euler_angle = ((D[1]*256+D[0])/16,(D[3]*256+D[2])/16,(D[5]*256+D[4])/16)
        
        return euler_angle
        
    
    def euler_velocity(self):
        '''This method uses the gyroscope sensor to output the rate of change for each of the
        Euler angles. These values are outputted in the tuple so that they follow the same order
        as the Euler angle - (Heading, Roll, Pitch).
        @D The raw data output of the sensor
        @euler_velocity A tuple containing the signed and converted angular velocities of the Heading, Roll, & Pitch (in degrees/sec)'''
        
        D = self.i2c.mem_read(6,self.address,self.GYR_DATA_XYZ) #Raw Data
        #"D" is a 6 byte list of raw gyro data
        #X: [0] LSB, [1] MSB --> d/dt Pitch
        #Y: [2] LSB, [3] MSB --> d/dt Roll
        #Z: [4] LSB, [5] MSB --> d/dt Heading
        #NOTE: To align output with Euler angles, need to swap X and Z order in tuple
        
        #Run list, D, through the signed method to sign integers
        D=IMU.signed(D)   
        
        #The order in the tuple is rearragned to (Z,Y,X) to align with (Header, pitch, roll)
        euler_velocity = ((D[5]*256+D[4])/16,(D[3]*256+D[2])/16,(D[1]*256+D[0])/16)
        return euler_velocity 
    
if __name__ == '__main__':
    
    #Creates I2C master object on Microcontroller to communicate with I2C slave
    i2c = pyb.I2C(1,pyb.I2C.MASTER)
    # Bus 1 -> (Pins PB8(clock) & PB9(Data))
    
    #Initializes BNO055 sensor object (instance of IMU class) to communicate with master
    imu = IMU(i2c,0x28)
    
    #Enable the IMU object
    imu.enable()

    
    
    while True:
        pyb.delay(1000)
        print('Calibration: ',imu.calib_status())
        print('Euler Angle: ',imu.euler_angle())
        print('Euler Veloc: ',imu.euler_velocity())
        
        
    
        
    
    