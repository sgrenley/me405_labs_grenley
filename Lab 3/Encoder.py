'''@file Encoder.py
This script defines the class MotorEncoder and contains a test program in the main loop
that will only run if this script is executed as a standalone program.
@author Spencer Grenley
@date 5/5/20
'''
import pyb

class MotorEncoder:
    '''This class implements a quadrature (2 phase) motor encoder for the ME405 board'''

    def __init__ (self, ENCA_pin, ENCB_pin, period, delay, timer):
        
        '''This method initializes the users inputs to produce an instance of the Encoder object
        
        @param ENCA_pin pyb.Pin object that takes channel 1 input from phase A of the quadrature encoder 
        @param ENCB_pin pyb.Pin object that takes channel 2 input from phase B of the quadrature encoder
        @param period is the length of the counter. For a 16-bit counter, this should equal "65535"
            This value is used when detecting and correcting for underflow or overflow
        @param delay is the user input delay, in milli-seconds.
            This value is currently unused in a method within this class, but is included for future use in determining speed
        @param timer is the correct timer that ENCA and ENCB pins need to be on Channel 1 and Channel 2
            Encoder pins PB6 & PB7 use tim 4 for channels 1 & 2
            Encoder pins PC6 & PC7 use tim 8 for channels 1 & 2 '''
        
        #print ('Creating a motor encoder...') #Decomment so see if method begins
        
        #Apply user input object variables to corresponding class attributes 
        self.ENCA_pin = ENCA_pin
        self.ENCB_pin = ENCB_pin
        self.period = period
        self.delay_ms = delay
        self.timer = timer
        
        #Create timer channels
            #want timer triggered by either channel --> tim.ENC_AB
        self.timch1 = self.timer.channel(1, pyb.Timer.ENC_AB, pin=self.ENCA_pin) 
        self.timch2 = self.timer.channel(2, pyb.Timer.ENC_AB, pin=self.ENCB_pin)
    
        #Set initial values for parameters used in methods
        
        self.pos_raw = 0             #Raw/uncorrected current encoder position
        self.pos_raw_previous = 0    #Previous uncorrected encoder position
        self.pos_corrected = 0       #Corrected current position
        
        self.delta = 0               #Corrected Delta (corrected for overflow or underflow)
            
        #print ('Motor encoder created!') #Decomment so see if method finishes
        
    def update(self):
        '''When called, this method updates the recorded position of the encoder.
                - This method stores the previous position, and checks and corrects for underflow and overflow
                - This method also produces the corrected delta value'''
        
        #print ('Updating position...') #Uncomment so see if method begins
        #-------------------------
        
        self.pos_raw_previous = self.pos_raw    #This stores the previous raw encoder position to be used in a delta
        self.pos_raw = self.timer.counter()     #This captures the current raw encoder position 
        
        delta_raw = self.pos_raw - self.pos_raw_previous #calculates the difference between previous and current uncorrected encoder positions
        
    #CORRECT delta -> Create self.delta
        if delta_raw > self.period/2:
            # Underflow has occured
            self.delta = delta_raw - self.period #Corrects underflow
        elif delta_raw < -self.period/2:
            # Overflow has occured
            self.delta = delta_raw + self.period #Corrects overflow
        else:
            self.delta = delta_raw #No correction needed for delta
    
    #Take corrected delta value, and 'integrate' (add instance to) to find corrected current position
        self.pos_corrected += self.delta
        
        #---------------------------------
        #print ('Position updated!') #Uncomment so see if method finishes
        

    def getposition(self):
        '''This method returns the most recently updated position of the encoder'''
        return self.pos_corrected
      
    def setposition(self, position_new):
        '''@param position_new Represents the users desired new position
        This method resets the current position to the users desired new position'''
        #print('Resetting to new postion...') #Uncomment to see if method begins
        #resets the position to to a specified value
        self.position_new = position_new
        self.pos_corrected = self.position_new
        #print('Position reset!') #Uncomment to see if method finishes
        
    def getdelta(self):
        '''This method returns the corrected difference in recorded positions between the two most recent update() calls'''
        return self.delta
        
        
        
if __name__ == '__main__':
    
    ENCA_pin = pyb.Pin(pyb.Pin.cpu.B6)
    ENCB_pin = pyb.Pin(pyb.Pin.cpu.B7)
    tim_period = 65535
    tim_delay_ms = 2000 #Delay in miliseconds
    
    #Encoder pins PB6 & PB7 use tim 4 for channels 1 & 2
    #Encoder pins PC6 & PC7 use tim 8 for channels 1 & 2
    tim = pyb.Timer(4, prescaler=0, period= tim_period) #Using PB6 and PB7
    
    #Initialize encoder instance
    encoder1 = MotorEncoder(ENCA_pin, ENCB_pin, tim_period, tim_delay_ms, tim)
        
    while True:
        pyb.delay(tim_delay_ms) #Creates the time delay (set above)
        
        encoder1.update()             #Update encoder
        print(encoder1.getposition()) #print the current position
            
        

         
    

